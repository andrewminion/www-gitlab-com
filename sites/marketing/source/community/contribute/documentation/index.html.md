---
layout: markdown_page
title: "Contributing to Documentation"
description: "This section pertains to documentation changes that are independent of other code/feature changes."
canonical_path: "/community/contribute/documentation/"
---

## Documentation

This section pertains to documentation changes that are independent of other code or feature changes. For documentation changes that accompany changes to code or features, see the [Contributing to Development page](/community/contribute/development/index.html).

1. If you do not have a specific page in mind and you are looking to get started contributing to documentation, feel free to pick one of the [documentation issues curated specifically for new contributors](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=good%20for%20new%20contributors&label_name%5B%5D=Accepting%20merge%20requests&label_name%5B%5D=documentation)
1. Alternatively, visit [docs.gitlab.com](https://docs.gitlab.com) for the latest documentation on GitLab, Charts, Omnibus, Runner and others, or the specific documentation page which you'd like to modify.
1. See the [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html) and [Writing Documentation](https://docs.gitlab.com/ee/development/documentation/) pages for more important details on writing documentation for GitLab.
1. Click the "Edit this page" link at the bottom of the page, fork the relevant project, and modify the documentation in GitLab’s web editor. Alternatively, you can fork the relevant project locally and edit the corresponding file(s) in its `/doc` or `/docs` path.
1. Open a merge request and remember to follow [branch naming conventions](https://docs.gitlab.com/ee/development/documentation/index.html#branch-naming) if you forked the project locally.
1. Mention `@gl-docsteam` in a comment, then wait for a review. You may need to change things if a reviewer requests it.
1. Get your changes merged!
